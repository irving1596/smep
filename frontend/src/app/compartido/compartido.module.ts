import { NgModule } from '@angular/core';
import { CompartidoComponent } from './compartido.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppComponent } from '../app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AppRoutingModule } from '../app-routing.module';
import { HeaderadmComponent } from './headeradm/headeradm.component';




@NgModule({

    declarations: [


        HeaderComponent,
        FooterComponent,
        SidebarComponent,
        HeaderadmComponent,
        HeaderadmComponent,

    ],
    exports: [

        HeaderComponent,
        FooterComponent,
        SidebarComponent,
        HeaderadmComponent
    ],
    imports: [
        AppRoutingModule,
        MDBBootstrapModule.forRoot(),
    ],
    providers: [],
    bootstrap: [AppComponent],
 


})

export class CompartidoModule { }
