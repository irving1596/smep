import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule } from '@angular/common/http';
import {ApiService} from './services/api.service';
import {ClaseService} from './services/clase.service';
import {EstudianteService} from './services/estudiante.service';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Rutas
import { APP_ROUTES } from './app.routes';

// modulos
import { MaterialModule } from './material.module';
import { PagesModule } from './pages/pages.module';

// Rutas
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompartidoComponent } from './compartido/compartido.component';
// import { EstudirComponent } from './admin/admi/estudir/estudir.component';
 //import { ClasesComponent } from './pages/admin/clases/clases.component';




@NgModule({
  declarations: [
    AppComponent,
    CompartidoComponent,
     //EstudirComponent,
  //   ClasesComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    APP_ROUTES,
    PagesModule,
    MaterialModule,
    HttpClientModule,


  ],
  exports: [


  ],

  providers: [ApiService,ClaseService,EstudianteService],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
