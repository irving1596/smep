// este es el lugar donde todos los servicos esyan centrealizados
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModalserviceService } from '../pages/admin/modalcreac/modalservice.service';




@NgModule({
  imports: [
    CommonModule,
    HttpClientModule // importamos este modulo para poder trabajar con este modulo
  ],
  providers: [

 ModalserviceService
  ],
  declarations: []
})
export class ServiceModule { }
