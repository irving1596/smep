import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { EstudianteComponent } from './pages/estudiante/estudiante.component';
import { ProfesorComponent } from './pages/profesor/profesor.component';
import { Nestudi1Component } from './pages/unidades/nestudi1/nestudi1.component';
import { Nestudi2Component } from './pages/unidades/nestudi2/nestudi2.component';
import { Nestudi3Component } from './pages/unidades/nestudi3/nestudi3.component';
import { Profesor1Component } from './pages/profesor/pevatema/profesor1.component';
import { Profesor2Component } from './pages/profesor/pevaestud/profesor2.component';
import { U1tema1Component } from './pages/utema/u1tema1/u1tema1.component';

import { U1tem1eje1Component } from './pages/utemaeje/u1tem1eje1/u1tem1eje1.component';
import { RegistroprofComponent } from './pages/admin/registroprof/registroprof.component';
import { AdmiComponent } from './pages/admin/admi/admi.component';
import { RegistroestudComponent } from './pages/admin/registroestud/registroestud.component';
import { UnidadesComponent } from './pages/unidades/unidades.component';

// Paginas de Tema de unidades

import { U1tema2Component } from './pages/utema/u1tema2/u1tema2.component';
import { U1tema3Component } from './pages/utema/u1tema3/u1tema3.component';
import { U1tema4Component } from './pages/utema/u1tema4/u1tema4.component';
import { U2tema1Component } from './pages/utema/u2tema1/u2tema1.component';
import { U3tema1Component } from './pages/utema/u3tema1/u3tema1.component';
import { U3tema2Component } from './pages/utema/u3tema2/u3tema2.component';
import { U3tema3Component } from './pages/utema/u3tema3/u3tema3.component';
import { U3tema4Component } from './pages/utema/u3tema4/u3tema4.component';
import { U3tema5Component } from './pages/utema/u3tema5/u3tema5.component';
import { U3tema6Component } from './pages/utema/u3tema6/u3tema6.component';

// Paginas de ejercicos

import { U1tem2eje1Component } from './pages/utemaeje/u1tem2eje1/u1tem2eje1.component';
import { U1tem3eje1Component } from './pages/utemaeje/u1tem3eje1/u1tem3eje1.component';
import { U1tem4eje1Component } from './pages/utemaeje/u1tem4eje1/u1tem4eje1.component';
import { U2tem1eje1Component } from './pages/utemaeje/u2tem1eje1/u2tem1eje1.component';
import { U3tem1eje1Component } from './pages/utemaeje/u3tem1eje1/u3tem1eje1.component';
import { U3tem2eje1Component } from './pages/utemaeje/u3tem2eje1/u3tem2eje1.component';
import { U3tem3eje1Component } from './pages/utemaeje/u3tem3eje1/u3tem3eje1.component';
import { U3tem4eje1Component } from './pages/utemaeje/u3tem4eje1/u3tem4eje1.component';
import { U3tem5eje1Component } from './pages/utemaeje/u3tem5eje1/u3tem5eje1.component';
import { U3tem6eje1Component } from './pages/utemaeje/u3tem6eje1/u3tem6eje1.component';
import { ClasesComponent } from './pages/admin/clases/clases.component';
import { Regisprof1Component } from './pages/admin/regisprof1/regisprof1.component';
import { Regisest1Component } from './pages/admin/regisest1/regisest1.component';
import { U1tem1eje2Component } from './pages/utemaeje/u1tem1eje2/u1tem1eje2.component';
import { U1tem1eje3Component } from './pages/utemaeje/u1tem1eje3/u1tem1eje3.component';
import { U1tem2eje2Component } from './pages/utemaeje/u1tem2eje2/u1tem2eje2.component';







const appRoutes: Routes = [


    { path: '',
    component: PagesComponent,
    children: [
    { path: 'login', component: LoginComponent },
    { path: '', redirectTo: './login'  , pathMatch:  'full' } // ruta que no tiene nada que redirecione al login

    ]


    }, // ruta principal

    { path: 'estudiante', component: EstudianteComponent, },
    { path: 'nestudi1', component: Nestudi1Component },
    { path: 'nestudi2', component: Nestudi2Component },
    { path: 'nestudi3', component: Nestudi3Component },


    // rutas de explicacion de temas


    { path: 'u1tema1', component: U1tema1Component },
    { path: 'u1tema2', component: U1tema2Component },
    { path: 'u1tema3', component: U1tema3Component },
    { path: 'u1tema4', component: U1tema4Component },
    { path: 'u2tema1', component: U2tema1Component },
    { path: 'u3tema1', component: U3tema1Component },
    { path: 'u3tema2', component: U3tema2Component },
    { path: 'u3tema3', component: U3tema3Component },
    { path: 'u3tema4', component: U3tema4Component },
    { path: 'u3tema5', component: U3tema5Component },
    { path: 'u3tema6', component: U3tema6Component },



    // rutas de ejerccios

    { path: 'u1tem1eje1', component: U1tem1eje1Component },
    { path: 'u1tem1eje2', component: U1tem1eje2Component },
    { path: 'u1tem1eje3', component: U1tem1eje3Component },
    { path: 'u1tem2eje1', component: U1tem2eje1Component },
    { path: 'u1tem2eje2', component: U1tem2eje2Component },
    { path: 'u1tem3eje1', component: U1tem3eje1Component },
    { path: 'u1tem4eje1', component: U1tem4eje1Component },
    { path: 'u2tem1eje1', component: U2tem1eje1Component },
    { path: 'u3tem1eje1', component: U3tem1eje1Component },
    { path: 'u3tem2eje1', component: U3tem2eje1Component },
    { path: 'u3tem4eje1', component: U3tem3eje1Component },
    { path: 'u3teme4je1', component: U3tem4eje1Component },
    { path: 'u3tem5eje1', component: U3tem5eje1Component },
    { path: 'u3tem6eje1', component: U3tem6eje1Component },




    // rutas de explicaciones
    { path: 'u1tema1', component: U1tema1Component },


    // rutas del adminnistar


    { path: 'registroprof', component: RegistroprofComponent},
    { path: 'registroestud', component: RegistroestudComponent },
    { path: 'admin', component: AdmiComponent},
    { path: 'clases', component: ClasesComponent},
    { path: 'regisprof1', component: Regisprof1Component},
    { path: 'regisest1', component: Regisest1Component},



    { path: 'profesor',
     component: ProfesorComponent,
     children: [
            { path: 'unidades', component: UnidadesComponent, data: {titulo: 'Mantenimiento de Usuario'} },
    ]
    },
    { path: 'profesor1', component: Profesor1Component },
    { path: 'profesor2', component: Profesor2Component },

    ];


export const APP_ROUTES = RouterModule.forRoot( appRoutes, { useHash: true } );
