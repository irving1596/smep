import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-u1tem1eje1',
  templateUrl: './u1tem1eje1.component.html',
  styleUrls: ['./u1tem1eje1.component.scss']
})
export class U1tem1eje1Component implements OnInit {

  respuesta = 0;

  a = false;
  b = true;
  c = true;
  d = true;

  constructor() { }

  ngOnInit() {
  }

  respuest( respuesta: number ) {

  console.log( respuesta );

  }
  activate(elem) {
    // deactivate all first
    this.a = false;
    this.b = false;
    this.c = false;
    this.d = false;

    switch (elem) {
      case 't1': {this.a = false;
                  console.log(this.a);
                  console.log('A');
                  break; }
      case 't2': {this.b = true;
                  console.log(this.b);
                  console.log('B');
                  break; }
      case 't3': {this.c = true;
                  console.log('C');
                  break; }
      case 't4': {this.d = true;
                  console.log('D');
                  break; }
    }
  }

}

