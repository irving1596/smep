import { Component, OnInit } from '@angular/core';
import { Usuario} from 'src/app/class/user.class';
@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})

export class ProfesorComponent implements OnInit {
  usuarios: Usuario[] = [];
  constructor() {
    if(localStorage.getItem('usuarios')){
          this.usuarios = JSON.parse(localStorage.getItem('usuarios'));
        }
      }

  ngOnInit() {
  }

}
