import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nestudi1Component } from './nestudi1.component';

describe('Nestudi1Component', () => {
  let component: Nestudi1Component;
  let fixture: ComponentFixture<Nestudi1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nestudi1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nestudi1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
