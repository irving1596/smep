import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nestudi2Component } from './nestudi2.component';

describe('Nestudi2Component', () => {
  let component: Nestudi2Component;
  let fixture: ComponentFixture<Nestudi2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nestudi2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nestudi2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
