import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nestudi3Component } from './nestudi3.component';

describe('Nestudi3Component', () => {
  let component: Nestudi3Component;
  let fixture: ComponentFixture<Nestudi3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nestudi3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nestudi3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
