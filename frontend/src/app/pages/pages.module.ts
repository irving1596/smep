
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompartidoModule } from '../compartido/compartido.module';
import { MaterialModule } from '../material.module';
import { PagesComponent } from './pages.component';

// ng2 -charts
import { ChartsModule } from 'ng2-charts';


import { EstudianteComponent } from './estudiante/estudiante.component';
import { ProfesorComponent } from './profesor/profesor.component';
import { Nestudi1Component } from './unidades/nestudi1/nestudi1.component';
import { Nestudi2Component } from './unidades/nestudi2/nestudi2.component';
import { Nestudi3Component } from './unidades/nestudi3/nestudi3.component';
import { Header2Component } from '../compartido/header2/header2.component';
import { Profesor1Component } from './profesor/pevatema/profesor1.component';
import { Profesor2Component } from './profesor/pevaestud/profesor2.component';
import { U1tema1Component } from './utema/u1tema1/u1tema1.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppComponent } from '../app.component';
import { AppRoutingModule } from '../app-routing.module';
import { LoginComponent } from '../login/login.component';
import { FormsModule } from '@angular/forms';
import { Graficas1Component } from './graficas1/graficas1.component';
import { U1tem1eje1Component } from './utemaeje/u1tem1eje1/u1tem1eje1.component';
import { RegistroprofComponent } from './admin/registroprof/registroprof.component';
import { AdmiComponent } from './admin/admi/admi.component';
import { RegistroestudComponent } from './admin/registroestud/registroestud.component';
import { UnidadesComponent } from './unidades/unidades.component';
import { UtemaComponent } from './utema/utema.component';
import { UtemaejeComponent } from './utemaeje/utemaeje.component';
import { U1tema2Component } from './utema/u1tema2/u1tema2.component';
import { U1tema3Component } from './utema/u1tema3/u1tema3.component';
import { U1tema4Component } from './utema/u1tema4/u1tema4.component';
import { U2tema1Component } from './utema/u2tema1/u2tema1.component';
import { U3tema1Component } from './utema/u3tema1/u3tema1.component';
import { U3tema2Component } from './utema/u3tema2/u3tema2.component';
import { U3tema3Component } from './utema/u3tema3/u3tema3.component';
import { U3tema4Component } from './utema/u3tema4/u3tema4.component';
import { U3tema5Component } from './utema/u3tema5/u3tema5.component';
import { U3tema6Component } from './utema/u3tema6/u3tema6.component';
import { U1tem2eje1Component } from './utemaeje/u1tem2eje1/u1tem2eje1.component';
import { U1tem3eje1Component } from './utemaeje/u1tem3eje1/u1tem3eje1.component';
import { U1tem4eje1Component } from './utemaeje/u1tem4eje1/u1tem4eje1.component';
import { U2tem1eje1Component } from './utemaeje/u2tem1eje1/u2tem1eje1.component';
import { U3tem1eje1Component } from './utemaeje/u3tem1eje1/u3tem1eje1.component';
import { U3tem2eje1Component } from './utemaeje/u3tem2eje1/u3tem2eje1.component';
import { U3tem3eje1Component } from './utemaeje/u3tem3eje1/u3tem3eje1.component';
import { U3tem4eje1Component } from './utemaeje/u3tem4eje1/u3tem4eje1.component';
import { U3tem5eje1Component } from './utemaeje/u3tem5eje1/u3tem5eje1.component';
import { U3tem6eje1Component } from './utemaeje/u3tem6eje1/u3tem6eje1.component';
import { ClasesComponent } from './admin/clases/clases.component';
import { Regisprof1Component } from './admin/regisprof1/regisprof1.component';
import { ModalcreacComponent } from './admin/modalcreac/modalcreap.component';
import { Regisest1Component } from './admin/regisest1/regisest1.component';
import { U1tem1eje2Component } from './utemaeje/u1tem1eje2/u1tem1eje2.component';
import { U1tem1eje3Component } from './utemaeje/u1tem1eje3/u1tem1eje3.component';
import { U1tem2eje2Component } from './utemaeje/u1tem2eje2/u1tem2eje2.component';
import { ModalcreaprofComponent } from './admin/modalcreaprof/modalcreaprof.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import {MatRadioModule} from '@angular/material';
import { U1tem3eje2Component } from './utemaeje/u1tem3eje2/u1tem3eje2.component';



@NgModule({

    declarations: [
        LoginComponent,
        PagesComponent,
        EstudianteComponent,
        ProfesorComponent,
        Nestudi1Component,
        Nestudi2Component,
        Nestudi3Component,
        Header2Component,
        Profesor1Component,
        Profesor2Component,
        U1tema1Component,
        Graficas1Component,
        U1tem1eje1Component,
        RegistroprofComponent,
        AdmiComponent,
        RegistroestudComponent,
        UnidadesComponent,
        UtemaComponent,
        UtemaejeComponent,
        U1tema2Component,
        U1tema3Component,
        U1tema4Component,
        U2tema1Component,
        U3tema1Component,
        U3tema2Component,
        U3tema3Component,
        U3tema4Component,
        U3tema5Component,
        U3tema6Component,
        U1tem2eje1Component,
        U1tem3eje1Component,
        U1tem4eje1Component,
        U2tem1eje1Component,
        U3tem1eje1Component,
        U3tem2eje1Component,
        U3tem3eje1Component,
        U3tem4eje1Component,
        U3tem5eje1Component,
        U3tem6eje1Component,
        ClasesComponent,
        Regisprof1Component,
         ModalcreacComponent,
         Regisest1Component,
         ModalcreaprofComponent,
         U1tem1eje2Component,
         U1tem1eje3Component,
         U1tem2eje2Component,
         U1tem3eje2Component

         // ModalserviceComponent,




    ],
    exports: [
        LoginComponent,
        EstudianteComponent,
        ProfesorComponent,
        Nestudi1Component,
        Nestudi2Component,
        Nestudi3Component,
        Header2Component,
        Profesor1Component,
        Profesor2Component,
        U1tema1Component,
        Graficas1Component,
        ClasesComponent,
        Regisprof1Component,
        ModalcreaprofComponent

    ],
    imports: [

        CompartidoModule,
        MDBBootstrapModule.forRoot(),
        AppRoutingModule,
        FormsModule,
        CommonModule,
        MaterialModule,









    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [ NO_ERRORS_SCHEMA ]



})
export class PagesModule { }
